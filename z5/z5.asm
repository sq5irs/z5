/*
 * z5.asm
 *
 *  Created: 26.05.2015 17:36:33
 *   Author: tracerout
 */ 
 
 Start:
 ldi r16, 0b00000000
 sts PORTf_DIR, r16

 ldi r17, 0b11111111
 sts PORTa_DIR, r17
 sts PORTc_DIR, r17
 
 main:
     clr r0
	 
	 cs:
		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharS << 1)
		 ldi ZL, low(CharS << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cy
	 rjmp cs

	 cy:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharY << 1)
		 ldi ZL, low(CharY << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cw
		 sbrs r18, 2
		 jmp cs
	 rjmp cy

	 cs2:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharS << 1)
		 ldi ZL, low(CharS << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp ct
		 sbrs r18, 2
		 jmp cy
	 rjmp cs2

	 ct:
	 	 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharT << 1)
		 ldi ZL, low(CharT << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp ce
		 sbrs r18, 2
		 jmp cs2
	 rjmp ct

	 ce:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharE << 1)
		 ldi ZL, low(CharE << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cm
		 sbrs r18, 2
		 jmp ct
	 rjmp ce

	 cm:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharM << 1)
		 ldi ZL, low(CharM << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cy2
		 sbrs r18, 2
		 jmp ce
	 rjmp cm

	 cy2:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharY << 1)
		 ldi ZL, low(CharY << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cw
		 sbrs r18, 2
		 jmp cm
	 rjmp cy2

	 cw:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharW << 1)
		 ldi ZL, low(CharW << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cb
		 sbrs r18, 2
		 jmp cy2
	 rjmp cw

	 cb:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharB << 1)
		 ldi ZL, low(CharB << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cu
		 sbrs r18, 2
		 jmp cw
	 rjmp cb

	 cu:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharU << 1)
		 ldi ZL, low(CharU << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cd
		 sbrs r18, 2
		 jmp cb
	 rjmp cu

	 cd:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharD << 1)
		 ldi ZL, low(CharD << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp co
		 sbrs r18, 2
		 jmp cu
	 rjmp cd

	 co:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharO << 1)
		 ldi ZL, low(CharO << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cw2
		 sbrs r18, 2
		 jmp cd
	 rjmp co

	 cw2:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharW << 1)
		 ldi ZL, low(CharW << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp ca
		 sbrs r18, 2
		 jmp co
	 rjmp cw2

	 ca:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharA << 1)
		 ldi ZL, low(CharA << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp cn
		 sbrs r18, 2
		 jmp cw2
	 rjmp ca

	 cn:
		 sbrs r18, 1
		 call w8_1

		 sbrs r18, 2
		 call w8_2

		 ldi ZH, high(CharN << 1)
		 ldi ZL, low(CharN << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 1
		 jmp ce2
		 sbrs r18, 2
		 jmp ca
	 rjmp cn

	 ce2:
		 sbrs r18, 1
		 call w8_1

		 ldi ZH, high(CharE << 1)
		 ldi ZL, low(CharE << 1);

		 call show

		 clr r18
		 lds r18, PORTf_IN
		 sbrs r18, 2
		 jmp cn
	 rjmp ce2
 rjmp main

 clear:
	 sts PORTa_OUT, r17
     sts PORTc_OUT, r16

	 sei
	 reti

 show_col:
	 adiw ZH:ZL,1
	 lpm;
	 sts PORTc_OUT, r0

	 adiw ZH:ZL,1
	 lpm;
     sts PORTa_OUT, r0

	 sei
	 reti

  show:
     call clear

	 /* first col */
	 lpm;
	 sts PORTc_OUT, r0

	 adiw ZH:ZL,1
	 lpm;
     sts PORTa_OUT, r0

	 /* rest */
     call clear
	 call show_col
     call clear
	 call show_col
	 call clear
	 call show_col
     call clear
	 call show_col

	 sei
	 reti

 w8_1:
	call clear
	clr r18
	lds r18, PORTf_IN
	sbrc r18, 1
	ret
	rjmp w8_1

w8_2:
	call clear
	clr r18
	lds r18, PORTf_IN
	sbrc r18, 2
	ret
	rjmp w8_2

 CharS: // c,a,c,a,c,a...
 .Db 0b0010000,0b01001111,0b0001000,0b00110110,0b0000100,0b00110110,0b0000010,0b00110110,0b0000001,0b01111001
 CharY:
 .Db 0b0010000,0b00011111,0b0001000,0b01100111,0b0000100,0b01110000,0b0000010,0b01100111,0b0000001,0b00011111
 CharW:
 .Db 0b0010000,0b00000001,0b0001000,0b01111110,0b0000100,0b01100001,0b0000010,0b01111110,0b0000001,0b00000001
 CharT:
 .Db 0b0010000,0b00111111,0b0001000,0b00111111,0b0000100,0b00000000,0b0000010,0b00111111,0b0000001,0b00111111
 CharU:
 .Db 0b0010000,0b00000001,0b0001000,0b01111110,0b0000100,0b01111110,0b0000010,0b01111110,0b0000001,0b00000001
 CharM:
 .Db 0b0010000,0b00000000,0b0001000,0b11011111,0b0000100,0b01101111,0b0000010,0b11011111,0b0000001,0b00000000
 CharE:
 .Db 0b0010000,0b00000000,0b0001000,0b00110110,0b0000100,0b00110110,0b0000010,0b00110110,0b0000001,0b00111110
 CharB:
 .Db 0b0010000,0b00000000,0b0001000,0b00110110,0b0000100,0b00110110,0b0000010,0b00110110,0b0000001,0b01001001
 CharD:
 .Db 0b0010000,0b00000000,0b0001000,0b00111110,0b0000100,0b00111110,0b0000010,0b00111110,0b0000001,0b01000001
 CharO:
 .Db 0b0010000,0b01000001,0b0001000,0b00111110,0b0000100,0b00111110,0b0000010,0b00111110,0b0000001,0b01000001
 CharN:
 .Db 0b0010000,0b00000000,0b0001000,0b10011111,0b0000100,0b01100111,0b0000010,0b11111001,0b0000001,0b00000000
 CharA:
 .Db 0b0010000,0b01000000,0b0001000,0b00111011,0b0000100,0b00111011,0b0000010,0b00111011,0b0000001,0b01000000